<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <div class="container">
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST['inputname'];
            $gender = $_POST['gender'];
            $phankhoa = $_POST['phankhoa'];
            $birthday = $_POST['ngaysinh'];
            $address = $_POST['address'];
            $image = $_FILES['image']['name'];
            $image_temp = $_FILES['image']['tmp_name'];

            move_uploaded_file($image_temp, "uploads/$image");
        }
        ?>
            <div class="form">
                <label for="inputname" class="style_name" style="margin-right: 150px;">Họ và tên </label>
                <?php echo "$name"; ?>
            </div>

            <div class="form">
                <label for="gioitinh" class="style_name style_gen" style="margin-right: 150px;">Giới tính </label>
                <?php echo "$gender"; ?>
            </div>
            
            <div class="form">
                <label for="phankhoa" class="style_name falcuty" style="margin-right: 150px;">Phân khoa </label>
                <?php echo "$phankhoa"; ?>
            </div>
            
            <div class="form">
                <label for="ngaysinh" class="style_name date_of_birth" style="margin-right: 150px;" >Ngày sinh </label>
                <?php echo "$birthday"; ?>
            </div>

            <div class="form">
                <label for="address" class="style_name" style="margin-right: 150px;" >Địa chỉ</label>
                <?php echo "$address"; ?>
            </div>

            <div class="form">
                <label for="image" class="style_image" style="margin-right: 150px;">Hình ảnh</label>
                <?php echo " <img src='uploads/$image' alt='Uploaded Image'></p>";?>
            </div>
            
            <div class="button-wrapper">
                    <input type="submit" class="button-container" id="submitButton" value="Xác nhận" style="margin-right: 245px;">
            </div>
    </div>
</body>

</html>