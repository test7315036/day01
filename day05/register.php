<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <div class="container">
        <div id="errorMessage"></div>
        <form class="form" method="POST" action="confirm.php" enctype="multipart/form-data">
            <div class="form">
                <label for="inputname" class="style_name">Họ và tên <span class="required">*</span></label>
                <input type="text" name="inputname" class="entering" required><br><br>
            </div>

            <div class="form">
                <label for="gioitinh" class="style_name style_gen">Giới tính <span class="required">*</span></label>
                <div class="picking_gender">
                    <label for="male">
                        <input type="radio" id="male" name="gender" value="Nam"> Nam
                    </label>
                    <label for="female">
                        <input type="radio" id="female" name="gender" value="Nữ"> Nữ
                    </label>
                </div>
            </div>

            <div class="form">
                <label for="phankhoa" class="style_name falcuty">Phân khoa <span class="required">*</span></label>
                <select name="phankhoa" class="picking_falcuty" required>
                    <option value="">-- Chọn phân khoa --</option>
                    <option value="KHMT">Khoa học máy tính</option>
                    <option value="KHDL">Khoa học dữ liệu</option>
                </select>
            </div>

            <div class="form">
                <label for="ngaysinh" class="style_name date_of_birth">Ngày sinh <span class="required">*</span></label>
                <input type="text" id="ngaysinh" name="ngaysinh" class="date_input" placeholder="dd/mm/yyyy" required>
            </div>

            <div class="form">
                <label for="address" class="style_name">Địa chỉ</label>
                <textarea id="address" name="address" class="input_address" rows="4" cols="30"></textarea>
            </div>

            <div class="form">
                <label for="image" class="style_image">Hình ảnh</label>
                <input type="file" name="image" id="imageUpload" class="uploadImage">
            </div>

            <div class="button-wrapper">
                <input type="submit" class="button-container" id="submitButton" value="Đăng ký">
            </div>
    </div>
</body>
<script>
    function formatDate(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }

        return day + "/" + month + "/" + year;
    }

    function parseDate(dateString) {
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10) - 1;
        var year = parseInt(parts[2], 10);

        var date = new Date(year, month, day);

        if (date.getDate() === day && date.getMonth() === month && date.getFullYear() === year) {
            return date;
        }

        return null; 
    }

    var ngaysinhInput = document.getElementById("ngaysinh");

    ngaysinhInput.addEventListener("blur", function() {
        var enteredDate = ngaysinhInput.value;
        var formattedDate = formatDate(parseDate(enteredDate));
        ngaysinhInput.value = formattedDate;
    });

    document.getElementById('submitButton').addEventListener('click', function() {
        var inputName = document.querySelector('.entering');
        var genderInputs = document.querySelectorAll('input[name="gender"]');
        var selectedGender = Array.from(genderInputs).find(input => input.checked);
        var selectedFaculty = document.querySelector('.picking_falcuty');
        var inputDate = document.querySelector('.date_input');
        var errorMessage = document.getElementById('errorMessage');

        errorMessage.innerHTML = '';

        if (inputName.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập tên.<br>';
        }
        if (!selectedGender) {
            errorMessage.innerHTML += 'Hãy chọn giới tính <br>';
        }
        if (selectedFaculty.value === '') {
            errorMessage.innerHTML += 'Hãy chọn phân khoa.<br>';
        }
        if (inputDate.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập ngày sinh<br>';
        } else {
            var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
            if (!dateRegex.test(inputDate.value)) {
                errorMessage.innerHTML += 'Hãy nhập ngày sinh đúng định dạng<br>';
            }
        }

        if (errorMessage.innerHTML !== '') {
            errorMessage.style.display = 'block';
        } else {
            errorMessage.style.display = 'none';
        }
    });
</script>

</html>