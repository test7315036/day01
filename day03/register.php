<?php
        $gioiTinhKhoa = [
            0 => 'Nam',
            1 => 'Nữ'
        ];
        $keys = array_keys($gioiTinhKhoa);
        $numOptions = count($gioiTinhKhoa);
        $khoaHoc = [
            '--Chọn phân khoa--',
            'MAT' => 'Khoa học máy tính',
            'KDL' => 'Khoa học vật liệu'
        ];

?>
<!DOCTYPE html>
<html>
<head>
    <title>Register Form</title>
    <style>
        .container {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 350px; 
            width: 600px;
            margin: 0; 
            background-color: white; 
            border: 1px solid blue;
            color: blue;
        }
        body {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 100vh; 
            margin: 0; 
            background-color: white; 
        }

        .register_form_name {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 90px; 
            height: 15px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block; 
        }

        .entering{
            border: 1px solid blue;
            color: blue;
            padding: 5px; 
            border-radius: 0px;
            width: 400px;   
            height: 25px;        
            display: inline-block;
            margin: 5px;
            
        }

        .register_form_gender {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 90px; 
            height: 15px;      
            padding-top: 9px;
            padding-bottom: 11px;
            margin-bottom: 24px;
            text-align: center;     
            display: inline-block;
        }

        .form {
            align-self: flex-start; 
            padding-left: 20px;
        }
        
        .picking_gender{
            color: black; 
            padding-left: 10px;
            margin-left: 12px;
            font-size: 20px;
            display: inline-block;
            
        }
        
        
        .register_form_falcuty {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 90px; 
            height: 19px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
        }
        .picking_falcuty {
            border: 1px solid blue;
            background-color: white; 
            color: black; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 180px;
            height: 41px;     
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
            margin-left: 17px;
        }

        .button-container {
            border: 1px solid blue;
            background-color: limegreen; 
            color: white; 
            padding: 5px; 
            border-radius: 5px;
            width: 150px;   
            height: 50px;  
            margin-top: 20px; 
            margin-left: 8px;      
        }
    </style>
</head>
<body>
    
    <div class="container">
        <form class="form"> 
                <label for="inputname" class = "register_form_name" >Họ và tên </label>
                <input type="text" id="inputname" name="inputname" class = "entering" required><br><br>
        </form>
        <form class="form">
            <label for="gioitinh" class="register_form_gender">Giới tính </label>
            <div class="picking_gender">
                <?php for ($i = 0; $i < $numOptions; $i++) {
                    $key = $keys[$i];
                    $gioiTinh = $gioiTinhKhoa[$key];
                ?>
                    <input type="radio" name="gioitinh" value="<?php echo $key; ?>">
                    <?php echo $gioiTinh; ?>
                <?php } ?>
            </div>
            
        </form>

        <form class="form">
            <label for="phankhoa" class = "register_form_falcuty " >Họ và tên </label>
            <select name="phankhoa" class = "picking_falcuty">
                <?php foreach ($khoaHoc as $key => $khoa) { ?>
                    <option value="<?php echo $key; ?>"><?php echo $khoa; ?></option>
                <?php } ?>
            </select>
        </form>

    <button class = "button-container"> Đăng ký </button>

    </div>
    
</body>
</html>
