<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "ltweb";

    $conn = new mysqli($servername, $username, $password, $database);

    if ($conn->connect_error) {
        die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
    }

    $name = $_POST['inputname'];
    $gender = $_POST['gender'];
    $faculty = $_POST['phankhoa']; // Corrected variable name
    $birthday = $_POST['ngaysinh'];
    $birthday = date('Y-m-d', strtotime(str_replace('/', '-', $birthday)));
    $address = $_POST['address'];
    $image = $_POST['image'];

    $insertSql = "INSERT INTO student (Name, Gender, Faculty, Date_of_birth, Address, Image) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($insertSql);
    $stmt->bind_param("ssssss", $name, $gender, $faculty, $birthday, $address, $image);

    $stmt->execute();

    $stmt->close();
    $conn->close();

    header("Location: list.php");
    exit();
}
