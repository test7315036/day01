<?php
// Include the database connection code
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Check if the ID parameter is provided in the URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Prepare and execute the delete statement
    $deleteSql = "DELETE FROM student WHERE ID = $id";
    if ($conn->query($deleteSql) === true) {
        echo "Xóa thành công!";
    } else {
        echo "Lỗi trong quá trình xóa: " . $conn->error;
    }
} else {
    echo "ID không được cung cấp.";
}

// Close the database connection
$conn->close();
