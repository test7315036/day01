CREATE DATABASE ltweb;

CREATE TABLE `student` (
  `ID` INT AUTO_INCREMENT PRIMARY KEY,
  `Name` varchar(100) NOT NULL,
  `Gender` varchar(30) NOT NULL,
  `Faculty` varchar(50) NOT NULL,
  `Date_of_birth` date NOT NULL,
  `Address` text NOT NULL,
  `Image` text NOT NULL
);