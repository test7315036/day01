<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Check if form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Check if reset button is clicked
    if (isset($_GET['reset'])) {
        // Reset search values to empty
        $searchFalcuty = "";
        $searchKeyword = "";
    } else {
        // Get search values
        $searchFalcuty = $_GET['falcuty'] ?? "";
        $searchKeyword = $_GET['keyword'] ?? "";
    }
} else {
    // Default values if not submitted
    $searchFalcuty = "";
    $searchKeyword = "";
}

// Build the SQL query based on search values
$selectSql = "SELECT * FROM student WHERE Falcuty LIKE '%$searchFalcuty%' AND Name LIKE '%$searchKeyword%'";
$result = $conn->query($selectSql);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List of Students</title>
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<style>
    .popup {
        display: none;
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
    }

    .popup-content {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 300px;
        height: 200px;
        background-color: #fff;
        border-radius: 5px;
        padding: 20px;
        margin: 0 auto;
        margin-top: 100px;
    }

    .popup-content h2 {
        margin: 0;
        margin-bottom: 20px;
        text-align: center;
    }

    .popup-content p {
        margin: 0;
        margin-bottom: 20px;
        text-align: center;
    }

    .popup-buttons {
        display: flex;
        justify-content: center;
    }

    .popup-buttons button {
        margin: 0 10px;
        padding: 10px 20px;
    }
</style>

<body>
    <div class="searching_info">
        <form id="searchForm" method="get" action="list.php">
            <label for="falcuty" class="style_name">Khoa </label>
            <input type="text" name="falcuty" id="falcuty" class="entering" value="<?php echo $searchFalcuty; ?>" required><br><br>

            <label for="keyword" class="style_name">Từ khóa </label>
            <input type="text" name="keyword" id="keyword" class="entering" value="<?php echo $searchKeyword; ?>" required><br><br>

            <div class="searching" style="margin-left: 190px;">
                <button type="button" id="resetBtn" class="custom-button">Reset</button>
            </div>
        </form>
    </div>

    <div class="numFound">
        <p id="totalStudentsFound">Số sinh viên tìm thấy: xxx</p>
    </div>


    <div class="adding"><a href="register.php"><button class="custom-button">Thêm</button></a></div>
    <table id="resultTable">
        <!-- Results will be displayed here using AJAX -->
    </table>
    <div id="confirmationPopup" class="popup">
        <div class="popup-content">
            <h2>Xác nhận xóa</h2>
            <p>Bạn có chắc chắn muốn xóa phần tử có ID <span class="id"></span>?</p>
            <div class="popup-buttons">
                <button onclick="hideConfirmation()">Hủy</button>
                <button onclick="deleteElement()"><a href="list.php">Xóa</a></button>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            // Function to perform AJAX search
            function performSearch() {
                var falcuty = $('#falcuty').val();
                var keyword = $('#keyword').val();

                $.ajax({
                    type: 'GET',
                    url: 'ajax_search.php',
                    data: {
                        falcuty: falcuty,
                        keyword: keyword
                    },
                    success: function(data) {
                        $('#resultTable').html(data);

                        // Update the total number of students found
                        var numFound = $('#resultTable tr').length; // Exclude the header row
                        $('#totalStudentsFound').text('Số sinh viên tìm thấy: ' + numFound);
                    }
                });
            }

            // Listen to keyup events on the search inputs
            $('#falcuty, #keyword').on('keyup', function() {
                performSearch();
            });

            // Handle reset button click
            $('#resetBtn').on('click', function() {
                $('#falcuty, #keyword').val('');
                performSearch();
            });

            // Initial search on page load
            performSearch(); // Perform initial search even when no criteria are entered
        });

        function deleteElement() {
            var idElement = document.querySelector("#confirmationPopup .id");
            var id = idElement.textContent;

            // Redirect to the delete page or perform the deletion via AJAX
            window.location.href = "delete.php?id=" + id;
        }
    </script>
</body>

</html>