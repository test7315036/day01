<?php
// Include the database connection code
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Get search values from AJAX request
$searchFaculty = $_GET['faculty'] ?? "";
$searchKeyword = $_GET['keyword'] ?? "";

// Build the SQL query based on search values
$selectSql = "SELECT ID, Name, Faculty FROM student WHERE Faculty LIKE '%$searchFaculty%' AND Name LIKE '%$searchKeyword%'";
$result = $conn->query($selectSql);

// Check if the query executed successfully
if ($result === false) {
    die("Lỗi trong khi thực thi truy vấn: " . $conn->error);
}

// Display the results in HTML format
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        // Add data attributes for counting
        echo "<tr data-faculty='" . htmlspecialchars($row["Faculty"]) . "' data-keyword='" . htmlspecialchars($row["Name"]) . "'>";
        echo "<td>" . $row["ID"] . "</td>"; // Add ID column
        echo "<td>" . $row["Name"] . "</td>";
        echo "<td>" . $row["Faculty"] . "</td>";
        echo "<td>";
        echo '<button style="background-color: #0074cc; color: #fff;" onclick="showConfirmation(' . $row["ID"] . ')">Xóa</button>';
        echo '<a href="update_student.php?id=' . $row["ID"] . '" style="background-color: #0074cc; color: #fff; margin-left: 15px;">Sửa</a>';
        echo "</td>";
        echo "</tr>";
    }
} else {
    echo "<tr><td colspan='4'>Không có kết quả tìm kiếm.</td></tr>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Student List</title>
    <script>
        function showConfirmation(id) {
            // Get the popup element
            var popup = document.getElementById("confirmationPopup");

            // Set the ID value in the popup
            var idElement = popup.querySelector(".id");
            idElement.textContent = id;

            // Show the popup
            popup.style.display = "block";
        }

        function hideConfirmation() {
            // Get the popup element
            var popup = document.getElementById("confirmationPopup");

            // Hide the popup
            popup.style.display = "none";
        }
    </script>
</head>

<body>
    <!-- Your HTML code for the table and other elements -->
</body>

</html>