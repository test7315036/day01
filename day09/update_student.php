<!-- update_students.php -->

<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";
$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_GET['id'] ?? $_POST['id'] ?? '';
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $faculty = $_POST['faculty'];
    $birthday = $_POST['ngaysinh'];
    $address = $_POST['address'];
    $image_temp = $_FILES['image']['tmp_name'];
    move_uploaded_file($image_temp, "uploads/$image");
    // Handle image update
    if ($_FILES['image']['name'] != '') {
        $image = $_FILES['image']['name'];
        $image_temp = $_FILES['image']['tmp_name'];

        move_uploaded_file($image_temp, "uploads/$image");
        $updateImageSql = "UPDATE student SET Image = 'uploads/$image' WHERE ID = $id";
        $conn->query($updateImageSql);
    }

    $updateSql = "UPDATE student SET Name = '$name', Gender = '$gender', Faculty = '$faculty', Date_of_birth = '$birthday', Address = '$address' WHERE ID = $id";
    if ($conn->query($updateSql) === true) {
        echo "Cập nhật thông tin sinh viên thành công.";
    
        // Redirect to list.php after successful update
        header("Location: list.php");
        exit(); // Ensure that no further code is executed after the redirect
    } else {
        echo "Lỗi cập nhật thông tin sinh viên: " . $conn->error;
    }
    
} else {
    $id = $_GET['id'] ?? $_POST['id'] ?? '';

    $selectSql = "SELECT * FROM student WHERE ID = $id";
    $result = $conn->query($selectSql);

    if ($result === false) {
        die("Lỗi trong khi thực thi truy vấn: " . $conn->error);
    }

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $name = $row["Name"];
        $gender = $row["Gender"];
        $faculty = $row["Faculty"];
        $birthday = $row["Date_of_birth"];
        $address = $row["Address"];
    } else {
        echo "Không tìm thấy sinh viên.";
        exit;
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cập nhật thông tin sinh viên</title>
    <link rel="stylesheet" href="./style.css">
<style>
input {
    border: none;
}
</style>
</head>

<body>
    <div class="container">
        <form method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <label for="name" class="style_name" style="margin-right: 150px;">Tên </label>
            <input type="text" name="name" value="<?php echo $name; ?>"><br><br>

            <label for="gender" class="style_name style_gen" style="margin-right: 150px;" >Giới tính</label>
            <input type="text" name="gender" value="<?php echo $gender; ?>"><br><br>
            
            <label for="faculty"  class="style_name falcuty" style="margin-right: 150px;">Phân khoa</label>
            <input type="text" name="faculty" value="<?php echo $faculty; ?>"><br><br>

            <label for="ngaysinh" class="style_name date_of_birth" style="margin-right: 150px;">Ngày sinh</label>
            <input type="text" name="ngaysinh"  value="<?php echo $birthday; ?>"><br><br>

            <label for="address"class="style_name" style="margin-right: 150px;">Địa chỉ</label>
            <input type="text" name="address" value="<?php echo $address; ?>"><br><br>

            <label for="image" class="style_image" style="margin-right: 150px;">Hình ảnh</label>
            
            <input type="file" name="image" id="imageUpload" class="uploadImage" value="<?php echo 'uploads/' . $image; ?>"><br><br>

            <input type="submit" name="submit" class="button-container" style="margin-left: 130px;" value="Xác nhận">
        </form>
    </div>
</body>

</html>
