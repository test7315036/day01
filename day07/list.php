<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

$selectSql = "SELECT * FROM student";
$result = $conn->query($selectSql);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List of Students</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <!-- <div class="container"> -->
    <div class="searching_info">
        <label for="inputname" class="style_name">Khoa </label>
        <input type="text" name="inputname" class="entering" required><br><br>
    </div>

    <div class="searching_info">
        <label for="inputname" class="style_name">Từ khóa </label>
        <input type="text" name="inputname" class="entering" required><br><br>
    </div>
    <div class="searching"><button class="custom-button">Tìm kiếm</button></div>



    <div class="numFound">
        <p>Số sinh viên tìm thấy: </p>
    </div>
    <div class="adding"><a href="register.php"><button class="custom-button">Thêm</button></a></div>
    <table>
        <tr>
            <th>No </th>
            <th>Tên sinh viên</th>
            <th>Phân khoa</th>
            <th> Action </th>
        </tr>
        <?php
        $rowNumber = 1;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $rowNumber . "</td>";
                echo "<td>" . $row["Name"] . "</td>";
                echo "<td>" . $row["Falcuty"] . "</td>";
                echo "<td>";
                echo '<button style="background-color: #0074cc; color: #fff;">Xóa</button>';
                echo '<button style="background-color: #0074cc; color: #fff; margin-left: 15px;">Sửa</button>';
                echo "</td>";
                echo "</tr>";
                $rowNumber++;
            }
        } else {
            echo "Không có dữ liệu sinh viên.";
        }
        ?>
    </table>
</body>

</html>