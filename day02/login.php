<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Web</title>
    <style>
        
        .login {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 150px; 
            height: 30px;      
            padding-top: 15px;     
            display: inline-block;
        }

        .entering{
            padding-top: 2px;
            border: 1px solid blue;
            color: blue;
            padding: 5px; 
            border-radius: 0px;
            width: 150px;   
            height: 30px;        
            display: inline-block;
            margin: 5px;
            padding-bottom: 11px;
            padding-top: 9px;
            
        }

        p {
            background-color: lightgray; 
            color: black; 
            padding: 5px; 
            width: 325px;
            height: 20px;
            border-radius: 0px;           
            display: inline-block;  
        }

        .button-container {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            border-radius: 5px;
            width: 150px;   
            height: 50px;  
            margin-top: 20px; 
            margin-left: 100px;      
        }

        .container {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 350px; 
            width: 600px;
            margin: 0; 
            background-color: white; 
            border: 1px solid blue;
            color: blue;
        }
        body {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 100vh; 
            margin: 0; 
            background-color: white; 
        }
        
    </style>
</head>
<body>
    
    <div class="container">
        <p> Bây giờ là: 
                <?php
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $data = ["Chủ Nhật","thứ 2","thứ 3","thứ 4","thứ 5","thứ 6","thứ 7"];
                $dateTime = new DateTime();
                $weeksday = date ('N');      
                $day = $dateTime -> format($weeksday);
                $time = date(' H:i ');
                echo $time; 
                echo ", ". $data[$weeksday];
                $vietnamTime = date(' d/m/Y ');        
                echo " ngày ".$vietnamTime;
        ?></p>

        <form action="login.php" method="POST">
            <label for="account" class = "login" >Tên đăng nhập </label>
            <input type="text" id="account" name="account" class = "entering" required><br><br>
            
            <label for="password" class = "login" >Mật khẩu </label>
            <input type="password" id="password" name="password" class = "entering" required><br><br>
            
            <button class = "button-container"> Đăng nhập </button>
        </form>
    </div>
    
        
</body>
</html>