<!DOCTYPE html>
<html>

<head>
    <title>Register Form</title>
    <style>
        .container,
        body {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin-top: 20px;
            background-color: white;
        }

        .container {
            width: 600px;
            border: 1px solid blue;
            color: blue;
        }

        .style_name,
        .entering,
        .style_gen,
        .falcuty,
        .picking_falcuty,
        .button-container,
        .date_of_birth,
        .date_input,
        .input_address {
            border: 1px solid blue;
            background-color: white;
        }

        .style_name {
            background-color: #70AD47;
            color: white;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 90px;
            height: 15px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
        }

        .entering {
            color: blue;
            padding: 5px;
            border-radius: 0px;
            width: 400px;
            height: 25px;
            display: inline-block;
            margin: 5px;
        }

        .style_gen {
            margin-bottom: 24px;
        }

        .picking_gender {
            color: black;
            padding-left: 10px;
            margin-left: -9px;
            font-size: 20px;
            display: inline-block;
        }

        .falcuty {
            margin-bottom: 24px;
        }

        .picking_falcuty {
            color: black;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 180px;
            height: 38px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
            margin-left: 5px;
        }

        .button-container {
            background-color: #70AD47;
            color: white;
            padding: 5px;
            border-radius: 5px;
            width: 150px;
            height: 50px;
            margin-top: 20px;
            margin-left: 8px;
            cursor: pointer ;
            border: none;
        }

        .picking_gender label {
            display: inline-block;
            margin-right: 10px;
            margin-top: 15px;
        }

        .required {
            color: red;
        }

        .date_of_birth {
            margin-bottom: 24px;
        }

        .date_input {
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 180px;
            height: 34px;
            padding-top: 0px;
            padding-bottom: 2px;
            text-align: center;
            display: inline-block;
            margin-left: 5px;
        }

        .input_address {
            border-radius: 0px;
            width: 400px;
            height: 100px;
            resize: vertical;
            padding-top: 0px;
            padding-bottom: 85px;
            display: inline-block;
            margin-left: 5px;
            padding-right: 10px;
            margin-top: 5px;
        }

        .form {
            display: flex;
            align-self: flex-start;
            padding-left: 20px;
        }

        #errorMessage {
            display: none;
            color: red;
            text-align: left; 
            padding-right: 400px;
        }
    </style>
</head>

<body>

    <div class="container">
    <div id="errorMessage"></div>
        <form class="form">
            <label for="inputname" class="style_name">Họ và tên <span class="required">*</span></label>
            <input type="text" name="inputname" class="entering" required><br><br>
        </form>

        <form class="form">
            <label for="gioitinh" class="style_name style_gen">Giới tính <span class="required">*</span></label>
            <div class="picking_gender">
                <label for="male">
                    <input type="radio" id="male" name="gender"> Nam
                </label>
                <label for="female">
                    <input type="radio" id="female" name="gender"> Nữ
                </label>
            </div>
        </form>

        <form class="form">
            <label for="phankhoa" class="style_name falcuty">Phân khoa <span class="required">*</span></label>
            <select name="phankhoa" class="picking_falcuty" required>
                <option value="">-- Chọn phân khoa --</option>
                <option value="KHMT">Khoa học máy tính </option>
                <option value="KHDL">Khoa học dữ liệu </option>
            </select>
        </form>

        <form class="form">
            <label for="ngaysinh" class="style_name date_of_birth">Ngày sinh <span class="required">*</span></label>
            <input type="text" id="ngaysinh" name="ngaysinh" class="date_input" placeholder="dd/mm/yyyy" required>
        </form>

        <form class="form">
            <label for="address" class="style_name"> Địa chỉ </label>
            <textarea id="address" name="address" class="input_address" rows="4" cols="30"></textarea>
        </form>



        <button class="button-container" id="submitButton"> Đăng ký </button>

    </div>

</body>
<script>
    function formatDate(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }

        return day + "/" + month + "/" + year;
    }

    function parseDate(dateString) {
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10) - 1;
        var year = parseInt(parts[2], 10);

        return new Date(year, month, day);
    }

    var ngaysinhInput = document.getElementById("ngaysinh");

    ngaysinhInput.addEventListener("blur", function() {
        var enteredDate = ngaysinhInput.value;
        var formattedDate = formatDate(parseDate(enteredDate));
        ngaysinhInput.value = formattedDate;
    });

    document.getElementById('submitButton').addEventListener('click', function() {
        var inputName = document.querySelector('.entering');
        var genderInputs = document.querySelectorAll('input[name="gender"]');
        var selectedGender = Array.from(genderInputs).find(input => input.checked);
        var selectedFaculty = document.querySelector('.picking_falcuty');
        var inputDate = document.querySelector('.date_input');
        var errorMessage = document.getElementById('errorMessage');

        errorMessage.innerHTML = ''; 

        if (inputName.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập tên.<br>';
        }
        if (!selectedGender) {
            errorMessage.innerHTML += 'Hãy chọn giới tính <br>';
        }
        if (selectedFaculty.value === '') {
            errorMessage.innerHTML += 'Hãy chọn phân khoa.<br>';
        }
        if (inputDate.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập ngày sinh<br>';
        } else {
            var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
            if (!dateRegex.test(inputDate.value)) {
                errorMessage.innerHTML += 'Hãy nhập ngày sinh đúng định dạng<br>';
            }
        }

        if (errorMessage.innerHTML !== '') {
            errorMessage.style.display = 'block';
        } else {
            errorMessage.style.display = 'none';
        }
    });
</script>

</html>