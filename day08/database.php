<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "ltweb";


    $conn = new mysqli($servername, $username, $password, $database);


    if ($conn->connect_error) {
        die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
    }

    $name = $_POST['inputname'];
    $gender = $_POST['gender'];
    $phankhoa = $_POST['phankhoa'];
    $birthday = $_POST['ngaysinh'];
    $birthday = date('Y-m-d', strtotime(str_replace('/', '-', $birthday)));
    $address = $_POST['address'];
    $image = $_POST['image'];

    $insertSql = "INSERT INTO student (Name, Gender, Falcuty, Date_of_birth, Address, Image) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($insertSql);
    $stmt->bind_param("ssssss", $name, $gender, $phankhoa, $birthday, $address, $image);

    $stmt->execute();
}
$sql = "SELECT * FROM student";
$result = $conn->query($sql);
if (isset($stmt)) {
    $stmt->close();
}
if (isset($conn)) {
    $conn->close();
}

header("Location: list.php");
exit();
