<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Check if form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Check if reset button is clicked
    if (isset($_GET['reset'])) {
        // Reset search values to empty
        $searchFalcuty = "";
        $searchKeyword = "";
    } else {
        // Get search values
        $searchFalcuty = $_GET['falcuty'] ?? "";
        $searchKeyword = $_GET['keyword'] ?? "";
    }
} else {
    // Default values if not submitted
    $searchFalcuty = "";
    $searchKeyword = "";
}

// Build the SQL query based on search values
$selectSql = "SELECT * FROM student WHERE Falcuty LIKE '%$searchFalcuty%' AND Name LIKE '%$searchKeyword%'";
$result = $conn->query($selectSql);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List of Students</title>
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>

<body>
    <div class="searching_info">
        <form id="searchForm" method="get" action="list.php">
            <label for="falcuty" class="style_name">Khoa </label>
            <input type="text" name="falcuty" id="falcuty" class="entering" value="<?php echo $searchFalcuty; ?>" required><br><br>

            <label for="keyword" class="style_name">Từ khóa </label>
            <input type="text" name="keyword" id="keyword" class="entering" value="<?php echo $searchKeyword; ?>" required><br><br>

            <div class="searching" style="margin-left: 190px;">
                <button type="button" id="resetBtn" class="custom-button">Reset</button>
            </div>
        </form>
    </div>

    <div class="numFound">
        <p id="totalStudentsFound">Số sinh viên tìm thấy: xxx</p>
    </div>


    <div class="adding"><a href="register.php"><button class="custom-button">Thêm</button></a></div>
    <table id="resultTable">
        <!-- Results will be displayed here using AJAX -->
    </table>

    <script>
        $(document).ready(function () {
        // Function to perform AJAX search
        function performSearch() {
            var falcuty = $('#falcuty').val();
            var keyword = $('#keyword').val();

            $.ajax({
                type: 'GET',
                url: 'ajax_search.php',
                data: {
                    falcuty: falcuty,
                    keyword: keyword
                },
                success: function (data) {
                    $('#resultTable').html(data);
                    
                    // Update the total number of students found
                    var numFound = $('#resultTable tr').length; // Exclude the header row
                    $('#totalStudentsFound').text('Số sinh viên tìm thấy: ' + numFound);
                }
            });
        }

        // Listen to keyup events on the search inputs
        $('#falcuty, #keyword').on('keyup', function () {
            performSearch();
        });

        // Handle reset button click
        $('#resetBtn').on('click', function () {
            $('#falcuty, #keyword').val('');
            performSearch();
        });

        // Initial search on page load
        performSearch(); // Perform initial search even when no criteria are entered
    });

    </script>
</body>

</html>
