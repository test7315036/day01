<?php
// Include the database connection code
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Get search values from AJAX request
$searchFalcuty = $_GET['falcuty'] ?? "";
$searchKeyword = $_GET['keyword'] ?? "";

// Build the SQL query based on search values
$selectSql = "SELECT * FROM student WHERE Falcuty LIKE '%$searchFalcuty%' AND Name LIKE '%$searchKeyword%'";
$result = $conn->query($selectSql);

// Display the results in HTML format
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        // Add data attributes for counting
        echo "<tr data-falcuty='" . htmlspecialchars($row["Falcuty"]) . "' data-keyword='" . htmlspecialchars($row["Name"]) . "'>";
        echo "<td>" . $row["Name"] . "</td>";
        echo "<td>" . $row["Falcuty"] . "</td>";
        echo "<td>";
        echo '<button style="background-color: #0074cc; color: #fff;">Xóa</button>';
        echo '<button style="background-color: #0074cc; color: #fff; margin-left: 15px;">Sửa</button>';
        echo "</td>";
        echo "</tr>";
    }
} else {
    echo "<tr><td colspan='3'>Không có kết quả tìm kiếm.</td></tr>";
}
?>
