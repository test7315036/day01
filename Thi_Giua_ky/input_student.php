<?php
        $gioiTinhKhoa = [
            0 => 'Nam',
            1 => 'Nữ'
        ];
        $keys = array_keys($gioiTinhKhoa);
        $numOptions = count($gioiTinhKhoa);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Register Form</title>
    <style>
        #year {
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <h1>Form đăng ký sinh viên</h1>
    <div class="container">
        <div id="error-message" style="color: red;"></div>
        <form class="form" id="name-form">
            <label for="inputname" class="register_form_name">Họ và tên</label>
            <input type="text" id="inputname" name="inputname" class="entering" required><br><br>
        </form>
        <form class="form" id="gender-form">
            <label for="gioitinh" class="register_form_gender">Giới tính</label>
            <div class="picking_gender">
                <?php for ($i = 0; $i < $numOptions; $i++) {
                    $key = $keys[$i];
                    $gioiTinh = $gioiTinhKhoa[$key];
                ?>
                <label class="radio-label">
                    <input type="radio" name="gioitinh" value="<?php echo $key; ?>">
                    <?php echo $gioiTinh; ?>
                </label>
                <?php } ?>
            </div>
        </form>

        <form id="dob-form" class="register_form_name>
            <h4>Ngày sinh</h4>
            <label for="year">Năm</label>
                <select id="year"></select>

                <label for="month">Tháng</label>
                <select id="month"></select>

                <label for="day">Ngày</label>
                <select id="day"></select>

        </form>
        <form class="register_form_name id="address-form">
            <label for="city">Địa Chỉ</label>
            <select id="city" onchange="updateSections()">
                <option value="">Thành phố</option>
                <option value="Hanoi">Hà Nội</option>
                <option value="Tp.HoChiMinh">Tp. Hồ Chí Minh</option>
            </select>
            <label for="section">Quận</label>
            <select id="section">
                <option value="">{blank}</option>
            </select>
        </form>
        <form class="form">
            <label for="moreinfor" class="style_name">Thông tin khác</label>
            <textarea id="moreinfor" name="moreinfor" class="input_moreinfor" rows="4" cols="30"></textarea>
        </form>
        <button class="button-container" onclick="validateForm()">Đăng ký</button>
    </div>

<script src="style.js"></script>
</body>
</html>
