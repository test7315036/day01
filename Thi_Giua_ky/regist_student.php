<!DOCTYPE html>
<html>
<head>
    <title>Registered Student Information</title>
</head>
<body>
    <h1>Registered Student Information</h1>
    <?php
    if (isset($_GET['name']) && isset($_GET['gender']) && isset($_GET['dob']) && isset($_GET['city']) && isset($_GET['section']) && isset($_GET['moreinfor'])) {
        $name = $_GET['name'];
        $gender = $_GET['gender'];
        $dob = $_GET['dob'];
        $city = $_GET['city'];
        $section = $_GET['section'];
        $moreinfor = $_GET['moreinfor']; // Get the textarea content

        // Define an array for gender options
        $genderOptions = [
            '0' => 'Nam',
            '1' => 'Nữ'
        ];

        echo "<p>Họ và tên: $name</p>";
        echo "<p>Giới tính: " . $genderOptions[$gender] . "</p>"; // Display the selected gender
        echo "<p>Ngày sinh: $dob</p>";
        echo "<p>Địa chỉ : $city - $section</p>";
        echo "<p>Thông tin khác: $moreinfor</p>"; // Display the textarea content
    } else {
        echo "Data not found.";
    }
    ?>
</body>
</html>
