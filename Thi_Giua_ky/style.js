var currentYear = new Date().getFullYear();
var yearSelect = document.getElementById("year");

// Set the date range for the year (this year - 40 to this year - 15)
for (var i = currentYear - 40; i >= currentYear - 15; i--) {
    var option = document.createElement("option");
    option.value = i;
    option.text = i;
    yearSelect.appendChild(option);
}
// Set the options for the month (01 to 12)
for (var i = 1; i <= 12; i++) {
    var option = document.createElement("option");
    option.value = i < 10 ? "0" + i : i;
    option.text = i < 10 ? "0" + i : i;
    document.getElementById("month").appendChild(option);
}

// Set the options for the day (01 to 31)
for (var i = 1; i <= 31; i++) {
    var option = document.createElement("option");
    option.value = i < 10 ? "0" + i : i;
    option.text = i < 10 ? "0" + i : i;
    document.getElementById("day").appendChild(option);
}
document.getElementById("day").value = "";
document.getElementById("month").value = "";
function updateSections() {
    var citySelect = document.getElementById("city");
    var sectionSelect = document.getElementById("section");
    var cityValue = citySelect.value;

    // Clear existing options
    sectionSelect.innerHTML = "";

    if (cityValue === "Hanoi") {
        addSectionOptions(sectionSelect, ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"]);
    } else if (cityValue === "Tp.HoChiMinh") {
        addSectionOptions(sectionSelect, ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]);
    } else {
        // If no city selected, show the default option
        addSectionOptions(sectionSelect, ["{blank}"]);
    }
}

function addSectionOptions(select, options) {
    for (var i = 0; i < options.length; i++) {
        var option = document.createElement("option");
        option.text = options[i];
        option.value = options[i];
        select.add(option);
    }
}

function validateForm() {
    var errorMessage = document.getElementById("error-message");
    errorMessage.innerHTML = ""; // Clear any previous error messages

    var name = document.getElementById("inputname").value;
    var genderElements = document.getElementsByName("gioitinh");
    var selectedGender = ""; // Variable to store the selected gender

    for (var i = 0; i < genderElements.length; i++) {
        if (genderElements[i].checked) {
            selectedGender = genderElements[i].value; // Store the selected gender value
            break;
        }
    }

    var year = document.getElementById("year").value;
    var month = document.getElementById("month").value;
    var day = document.getElementById("day").value;
    var city = document.getElementById("city").value;
    var section = document.getElementById("section").value; // Get the selected section
    var moreinfor = document.getElementById("moreinfor").value; // Get the textarea content

    var hasError = false; // Flag to check if there are validation errors

    // Check if the "Họ và tên" field is empty
    if (name.trim() === "") {
        errorMessage.innerHTML += "Hãy nhập họ tên.<br>";
        hasError = true;
    }

    // Check if a gender is selected
    if (selectedGender === "") {
        errorMessage.innerHTML += "Hãy chọn giới tính.<br>";
        hasError = true;
    }

    if (month === "" || day === "" ) {
        errorMessage.innerHTML += "Hãy chọn ngày sinh.<br>";
        hasError = true;
    }

    if (city === "") {
        errorMessage.innerHTML += "Hãy chọn địa chỉ.<br>";
        hasError = true;
    }

    if (hasError) {
        return false;
    }

    // All validations passed, construct the URL with query parameters
    var redirectURL = 'regist_student.php' +
        '?name=' + encodeURIComponent(name) +
        '&gender=' + encodeURIComponent(selectedGender) +
        '&dob=' + encodeURIComponent(year + '/' + month + '/' + day) +
        '&city=' + encodeURIComponent(city) +
        '&section=' + encodeURIComponent(section) + 
        '&moreinfor=' + encodeURIComponent(moreinfor); 
    window.location.href = redirectURL;
    return true;
}
